import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import * as serviceWorker from "./serviceWorker";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk"; // no changes here 😀
import rootReducer from "./apps/redux/reducers";
import RouteComponent from "./apps/route";
import { NavBar } from "./apps/component";
import { CartScreen } from "./apps/screens";
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));
export default class App extends React.Component {
  
  render() {
    console.log(store.getState())
    return (
      <React.Fragment>
        <Provider store={store}>
          <RouteComponent/>
        </Provider>
      </React.Fragment>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
