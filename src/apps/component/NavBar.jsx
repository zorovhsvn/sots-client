import React, { Component } from "react";
import {
  Container,
  Navbar,
  Nav,
  Form,
  Button,
  FormControl,
  Dropdown,
  DropdownButton
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { ensureArray } from "../utils/func";

export default class NavBarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDropDownCategory: false,
      showSearch: false,
      showLogin: false,
      dataSearch: [],
      keyWord: ""
    };

    this.timer = null;
  }

  render() {
    return (
      <React.Fragment>
        <Container fluid style={{
          paddingLeft:0,
          paddingRight:0,
        }}>
          <Navbar bg="light" expand="sm">
            <Navbar.Brand as={Link}  to="/">SOTS</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Form
                inline
                style={{
                  width: "30%"
                }}
              >
                <FormControl
                  style={{
                    width: "100%",
                    backgroundColor: "#CCCC"
                  }}
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  onChange={this.onChangeTextSearch}
                />

                {this.state.showSearch && this.state.keyWord != "" && (
                  <Dropdown
                    className="mr-sm-2"
                    style={{
                      width: "100%"
                    }}
                    show
                  >
                    <Dropdown.Menu
                      style={{
                        width: "100%"
                      }}
                    >
                      {ensureArray(this.state.dataSearch).length > 0 &&
                        ensureArray(this.state.dataSearch).map((values, i) => {
                          return (
                            <Dropdown.Item
                              key={i}
                              as={Link}
                              to={`/music/${values.title_ascii}/${values.id}`}
                            >
                              {values.title}
                            </Dropdown.Item>
                          );
                        })}
                      {ensureArray(this.state.dataSearch).length == 0 && (
                        <Dropdown.Item
                          disabled
                          style={{
                            textAlign: "center"
                          }}
                        >
                          Không tìm thấy
                        </Dropdown.Item>
                      )}
                    </Dropdown.Menu>
                  </Dropdown>
                )}
              </Form>
              <Nav className={"ml-auto"}>
                <Button onClick={this.props.goToLogin} variant={"link"} style={style.topLink}>
                  Tài khoản
                </Button>
                <hr className={"dash-vertical"} />
                <Button onClick={this.props.showCart} variant={"link"} style={style.topLink}>
                  Giỏ hàng 
                </Button>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Nav
            style={{
              display: "flex",
              justifyContent: "center",
              background: "rgba(0,0,0)"
            }}
          >
            {[0,1,2,3].map((data)=>{
              return(

            <li style={{
              padding:10,
              color:'white'
            }}>
              <span>{data}</span>
              {/* <Dropdown
                className="mr-sm-2"
                style={{
                  width: "100%",
                  position:'absolute',
                  left:0,
                  marginTop:10,
                  height:150,
                  flexWrap:'wrap',
                  whiteSpace:'normal'

                }}
                show
              >
                  <li style={{
                    width:"33%",
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%",
                    flexWrap:'wrap'
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>
                  <li style={{
                    width:"33%"
                  }}>
                  <Dropdown.Item as={Link}>
                    asdasdas
                  </Dropdown.Item>
                  </li>

              </Dropdown> */}
            </li>
              )
            })}
          </Nav>
        </Container>
      </React.Fragment>
    );
  }
}

const style = {
  topLink: {
    fontSize: 13,
    color: "#000000",
    textDecoration: "none"
  }
};
