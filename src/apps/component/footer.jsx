import React, { Component } from "react";
import { Container, Row, Col,Button } from "react-bootstrap";
import "./style.css"
import { Link } from "react-router-dom";
export default class FooterComponent extends Component {
  render() {
    return (
      <footer className="footer">
        <Container>
          <Row>
            <Col>
              <p>CONTACT CUSTOMER CARE</p>
              <p>Email us 24/7 at:</p>
              <p>support@flightclub.com</p>
            </Col>

            <Col>
              <p>@SOTS</p>
              <Button href={"https://www.facebook.com/Trong.Tin.248"} className={"mx-md-1"} variant={'outline-primary'}>
                  <i className={"fa fa-facebook"}/>
              </Button>
              <Button className={"mx-md-1"} variant={'outline-danger'}>
                  <i className={"fa fa-google"}/>
              </Button>

            </Col>

            <Col>
              <p>CONTACT CUSTOMER CARE</p>
              <p>Email us 24/7 at:</p>
              <p>support@flightclub.com</p>
            </Col>
          </Row>
        </Container>
        <hr className={"dash"} />
        <div className="footer-copyright text-center py-3">
        <Container fluid>
          &copy; {new Date().getFullYear()} Copyright: <Link to={"/"} > Sots </Link>
        </Container>
      </div>
      </footer>
    );
  }
}
