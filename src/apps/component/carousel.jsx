import React, { Component } from "react";
import { Carousel, Image,Button,Container } from "react-bootstrap";
import FadeIn from 'react-fade-in'
import { Link } from "react-router-dom";
import { ensureArray } from "../utils/func";
export default class CarouselComponent extends Component {
  renderTitle = (type, data) => {
    switch (type) {
      case "music":
        return data.title;
      case "album":
        return data.name;
      default:
        return "";
    }
  };

  renderDetailSub = (type, data) => {
    switch (type) {
      case "music":
        return data.singer;
      case "album":
        return data.info;
      default:
        return "";
    }
  };

  navigateScreen = value => {
    console.log(value)
    switch (value.type) {
      case "music":
        return `/music/${value.data_type.title_ascii}/${value.data_type.id}`;
      case "album":
        return `/album/${value.data_type.slug}/${value.data_type.id}`;
    }
  };
  render() {
    let { data } = this.props;
    return (
      <FadeIn delay={500}>

      <Carousel interval={3000}>
        {ensureArray(data).map(values => {
          return (
            <Carousel.Item>
              <Link
                className="d-block w-100"
                to={this.navigateScreen(values)}
                style={{
                  height: 400,
                  backgroundImage: `url(${
                    values.media
                      ? values.media
                      : "https://file.tinnhac.com/resize/600x-/music/2017/01/12/maxresdefault1-09b6.jpg"
                  })`,
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                  backgroundRepeat: "no-repeat"
                }}
              />
            </Carousel.Item>
          );
        })}
      </Carousel>
      </FadeIn>

    );
  }
}
