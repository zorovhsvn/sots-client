import CarouselComponent from './carousel'
import NavBar from './NavBar'
import FooterComponent from './footer'
export {
    CarouselComponent,
    NavBar,
    FooterComponent,
}