import React, { Component } from "react";
import { Button, Container, Image, Row, Col } from "react-bootstrap";
import "./style.css";
import posed from "react-pose";
import FadeIn from "react-fade-in";
import { Link } from "react-router-dom";
import { NavBar, FooterComponent } from "../../component";

const Sidebar = posed.div({
  open: {
    x: "-100%",
    delayChildren: 200,
    staggerChildren: 50
  },
  closed: { x: "0%" }
});

export default class layout extends Component {
  itemProduct = () => {
    return (
      <React.Fragment>
        <FadeIn delay={500}>
          <Link
            to={"/detail"}
            className={"product-details"}
            style={{
              textAlign: "center",
              backgroundColor: "#FFF",
              textDecoration: "none"
            }}
          >
            <Image
              thumbnail
              src={
                "https://thumbs.nosto.com/quick/h239k2p6/8/344297/82b573addfd423fe14f973e595128cbdb1296e58ccdccbe352a638c5ddc240efa/A"
              }
            />
            <Col>
              <p
                className={"product-detail-text"}
                style={{
                  fontWeight: "bold",
                  fontSize: 18
                }}
              >
                Air Jordan
              </p>

              <p
                className={"product-detail-text"}
                style={{
                  color: "#b1b1b1"
                }}
              >
                Detail Jordan
              </p>
              <span>$Price</span>
            </Col>
          </Link>
        </FadeIn>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <NavBar
          goToLogin={() => this.props.history.replace("/account")}
          showCart={() =>
            this.props.showCart
              ? this.props.actions.hideCart()
              : this.props.actions.showCart()
          }
        />
        <Container
          fluid
          style={{
            paddingRight: 20,
            paddingLeft: 20,
            backgroundColor: "#f7f7f7",
            width: "100%",
            height: "100%"
          }}
        >
          <div className={"d-flex justify-content-center mt-md-3"}>
            <h2>Category Name</h2>
          </div>

          <div
            style={{
              display: "flex",
              flexDirection: "row"
            }}
          >
            <div
              className={
                "actions-category-left d-flex flex-row justify-content-center"
              }
            >
              <span
                style={{
                  fontFamily: "Roboto Condensed",
                  alignSelf: "center",
                  fontWeight: "600",
                  color: "silver"
                }}
              >
                View
              </span>
              <div className={"btn-actions-limit"}>
                <Button
                  style={{
                    color: "#cccC",
                    textDecoration: "none"
                  }}
                  variant={"link"}
                >
                  10
                </Button>
                <Button
                  style={{
                    color: "#cccC",
                    textDecoration: "none"
                  }}
                  variant={"link"}
                >
                  30
                </Button>
                <Button
                  style={{
                    color: "#cccC",
                    textDecoration: "none"
                  }}
                  variant={"link"}
                >
                  50
                </Button>
              </div>
            </div>

            <div className={"actions-category-right"}></div>
          </div>

          <Row
            style={{
              marginLeft: 16,
              marginRight: 16
            }}
          >
            <Col
              style={{
                backgroundColor: "#FFF"
              }}
              md={3}
            >
              <div
                className={"total-item p-md-3 d-flex justify-content-center"}
              >
                <span>3492 ITEMS FOUND</span>
              </div>
              <div className={"btn-choose-category d-flex flex-wrap"}>
                <div className={"btn-filter-option"}>
                  <span>Boy</span>
                </div>
              </div>
              <hr className={"dash"} />

              <div className={"btn-choose-category d-flex flex-wrap"}>
                <div className={"btn-filter-option"}>
                  <span>Boy</span>
                </div>
              </div>
              <hr className={"dash"} />

              <div className={"btn-choose-category d-flex flex-wrap"}>
                <div className={"btn-filter-option"}>
                  <span>Boy</span>
                </div>
              </div>
              <hr className={"dash"} />
            </Col>
            <Col className={"d-flex flex-row flex-wrap"} md={9}>
              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>

              <div
                style={{
                  backgroundColor: "#fff"
                }}
                className={"col-md-4 p-md-3 mb-md-1"}
              >
                {this.itemProduct()}
              </div>
            </Col>
          </Row>
        </Container>

        <FooterComponent />
      </React.Fragment>
    );
  }
}
