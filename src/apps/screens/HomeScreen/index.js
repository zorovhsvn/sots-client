import Layout from "./layout";
import connectRedux from "../../redux/connect";

class HomeScreen extends Layout {
  constructor(props) {
    super(props);
    this.state = {};

    console.log(props)
  }
}

const mapStateToProps = (state) => {
  return {
    showCart:state.app.showCart
  };
};

export default connectRedux(mapStateToProps, HomeScreen);
