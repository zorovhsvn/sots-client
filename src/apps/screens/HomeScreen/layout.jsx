import React, { Component } from "react";
import { Container, Row, Col, Image, Button } from "react-bootstrap";
import { CarouselComponent, NavBar, FooterComponent } from "../../component";
import "./style.css";
import { Link } from "react-router-dom";
import FadeIn from "react-fade-in";

export default class Layout extends Component {
  itemProduct = () => {
    return (
      <React.Fragment>
        <FadeIn delay={500}>
          <Link
            to={"/detail"}
            className={"product-details"}
            style={{
              textAlign: "center",
              backgroundColor: "#FFF",
              textDecoration: "none"
            }}
          >
            <Image
              thumbnail
              src={
                "https://thumbs.nosto.com/quick/h239k2p6/8/344297/82b573addfd423fe14f973e595128cbdb1296e58ccdccbe352a638c5ddc240efa/A"
              }
            />
            <Col>
              <p
                className={"product-detail-text"}
                style={{
                  fontWeight: "bold",
                  fontSize: 18
                }}
              >
                Air Jordan
              </p>

              <p
                className={"product-detail-text"}
                style={{
                  color: "#b1b1b1"
                }}
              >
                Detail Jordan
              </p>
              <span>$Price</span>
            </Col>
          </Link>
        </FadeIn>
      </React.Fragment>
    );
  };

  renderCategoryFind = (name,urlImage)=>{
    return (
      <React.Fragment>
        <FadeIn delay={500}>
          <Link
            to={"/category"}
            className={"product-details"}
            style={{
              textAlign: "center",
              backgroundColor: "#FFF",
              textDecoration: "none"
            }}
          >
            <Image
              src={urlImage}
            />
            <Col style={{
              marginTop:15
            }}>
              <p
                className={"product-detail-text"}
              >
                {name}
              </p>
            </Col>
          </Link>
        </FadeIn>
      </React.Fragment>
    );
  }

  render() {
    return (
      <React.Fragment>
          <div>
            <NavBar
              goToLogin={() => this.props.history.replace("/account")}
              showCart={() =>
                this.props.showCart
                  ? this.props.actions.hideCart()
                  : this.props.actions.showCart()
              }
            />
          </div>
          <Container fluid style={{
            padding:0
          }}>
            <div className={"slider"}>
              <CarouselComponent data={[0, 1, 2, 3, 4]} />
            </div>
            <div style={{
              padding:"0 15px 0 15px"
            }} className={"Top-Sellers"}>
              <div className={"title-wrapper my-md-3"}>
                <h1>Top Sellers</h1>
              </div>
              <div className={"product-wrapper my-md-3"}>
                <Row>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                </Row>
                <FadeIn delay={500}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop:10,
                      marginBottom:10,
                    }}
                  >
                    <Button variant={"outline-dark"}>Xem tất cả</Button>
                  </div>
                </FadeIn>
              </div>
            </div>

            <hr className={"dash"} />

            <div style={{
              padding:"0 15px 0 15px"
            }} className={"Top-New"}>
              <div className={"title-wrapper my-md-3"}>
                <h1>New Arrivals</h1>
              </div>
              <div className={"product-wrapper my-md-3"}>
                <Row>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                  <Col
                    style={{
                      display: "flex",
                      justifyContent: "center"
                    }}
                    md={3}
                  >
                    {this.itemProduct()}
                  </Col>
                </Row>
              </div>
              <FadeIn delay={500}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop:10,
                      marginBottom:10,
                    }}
                  >
                    <Button variant={"outline-dark"}>Xem tất cả</Button>
                  </div>
                </FadeIn>
            </div>
          </Container>
          <Container fluid className={"Category-wrapper"}>
            <div className={"title-wrapper my-md-3"}>
              <h1>FIND YOUR PAIR</h1>
            </div>
            <div className={"product-wrapper my-md-3"}>
              <Row className={"d-flex justify-content-center"}>
                <Col
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    margin:10,
                    backgroundColor:'#fff',
                    paddingTop:10,
                    paddingBottom:10
                  }}
                  md={3}
                >
                  {this.renderCategoryFind("MEN","https://www.flightclub.com/media/wysiwyg/home/men_resized.jpg")}
                </Col>
                <Col
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    margin:10,
                    backgroundColor:'#fff',
                    paddingTop:10,
                    paddingBottom:10,
                  }}
                  md={3}
                >
                  {this.renderCategoryFind("WOMAN","https://www.flightclub.com/media/wysiwyg/home/women_resized.jpg")}
                </Col>
                <Col
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    margin:10,
                    backgroundColor:'#fff',
                    paddingTop:10,
                    paddingBottom:10
                  }}
                  md={3}
                >
                  {this.renderCategoryFind("CHILD","https://www.flightclub.com/media/wysiwyg/home/kids_resized.jpg")}
                </Col>
              </Row>
            </div>

          </Container>
          <FooterComponent />
      </React.Fragment>
    );
  }
}
