import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import "./style.css";
export default class layout extends Component {
  render() {
    return (
      <React.Fragment>
        <div className={"bgLogin"} />
        <Container
          fluid
          style={{
            padding: 0,
            margin: 0,
            position: "absolute",
            zIndex: 11,
            height: "100%",
          }}
        >
          <div
            className={
              "h-100 form-login d-flex align-items-center justify-content-center flex-column"
            }
          >
            <div className={"header"} style={{
              backgroundColor:'pink',
              width:"30%",
              display:'flex',
              justifyContent:'center',
              borderTopLeftRadius:10,
              borderBottomRightRadius:10,
              padding:10,
              position:'absolute',
              top:"25%",
              fontWeight:'600',
              fontSize:20
            }}>
              <span>Login admin</span>
            </div>
            <Form style={{
              backgroundColor:'rgba(255,255,255,0.5)',
              padding:20,
              borderRadius:10,
              width:"25%",
              boxShadow:5
            }}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Username</Form.Label>
                <Form.Control placeholder={"Username"} />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>

              <Button
              variant={'outline-info'}
                style={{
                  width: "100%"
                }}
              >
                Login
              </Button>
            </Form>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}
