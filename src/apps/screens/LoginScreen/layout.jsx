import React, { Component } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { NavBar, FooterComponent } from "../../component";
import "./style.css";
export default class layout extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar 
        goToLogin={() => this.props.history.replace("/account")}
        showCart={() =>
          this.props.showCart
            ? this.props.actions.hideCart()
            : this.props.actions.showCart()
        }
      />
        <Container
          style={{
            marginTop: 20,
            marginBottom: 20,
            padding: 10
          }}
        >
          <Row>
            <Col md={8}>
              <div className={"register px-md-3"}>
                <div className={"header-form"}>
                  <h4>CREATE AN ACCOUNT</h4>
                </div>
                <Row
                  className={"py-md-2"}
                  style={{
                    backgroundColor: "rgba(251,251,251,1)"
                  }}
                >
                  <Col md={6}>
                    <div className={"form-register"}>
                      <Form>
                        <Form.Group controlId="formBasicEmail">
                          <Form.Label>First Name *</Form.Label>
                          <Form.Control placeholder="First Name" required />
                          {/* <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                      </Form.Text> */}
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                          <Form.Label>Last Name *</Form.Label>
                          <Form.Control placeholder="Last name" required />
                        </Form.Group>

                        <Form.Group controlId="formBasicCheckbox">
                          <Form.Label>Email Address *</Form.Label>
                          <Form.Control
                            type={"email"}
                            placeholder="Email"
                            required
                          />
                        </Form.Group>

                        <Form.Group controlId="formBasicCheckbox">
                          <Form.Label>Password *</Form.Label>
                          <Form.Control
                            type={"password"}
                            placeholder="Last name"
                            required
                          />
                        </Form.Group>

                        <Form.Group controlId="formBasicCheckbox">
                          <Form.Label>Confirm Password *</Form.Label>
                          <Form.Control
                            placeholder="Confirm Password"
                            required
                          />
                        </Form.Group>

                        <Button
                          variant="outline-dark"
                          className={"w-100"}
                          type="submit"
                        >
                          Create Account
                        </Button>
                      </Form>
                    </div>
                  </Col>
                  <Col md={6}>
                    <h6 className={"headers-wrapper"}>MORE FEATURES! </h6>
                    <p className={"text-wrapper"}>
                      In the coming months, we're launching lots of special
                      functionality for registered users. Sign-up to get access
                      first.
                    </p>
                    <h6 className={"headers-wrapper"}>
                      SAVE YOUR CART TO ALL DEVICES!{" "}
                    </h6>
                    <p className={"text-wrapper"}>
                      You'll be able to see the content of your shopping cart on
                      all devices; phone, tablet and desktop.
                    </p>
                    <h6 className={"headers-wrapper"}>CHECK OUT QUICKLY! </h6>
                    <p className={"text-wrapper"}>
                      You'll be able to save your addresses, shipping
                      preferences and more.{" "}
                    </p>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col md={4} className={"px-md-3"}>
              <div className={"header-form"}>
                <h4>SIGN IN</h4>
              </div>
              <div
                style={{
                  backgroundColor: "rgb(251,251,251)"
                }}
                className={"form-signin p-md-3"}
              >
                <Form>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text>
                  </Form.Group>

                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                  </Form.Group>
                  <Button
                    variant="link"
                    style={{
                      color: "#000"
                    }}
                    className={"text-decoration-none w-100"}
                    type="submit"
                  >
                    Forget account
                  </Button>
                  <Button
                    variant="outline-dark"
                    className={"w-100"}
                    type="submit"
                  >
                    Login
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
        <FooterComponent />
      </React.Fragment>
    );
  }
}
