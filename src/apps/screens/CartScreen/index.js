import layout from "./layout";
import connectRedux from "../../redux/connect";

class CartScreen extends layout {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentWillReceiveProps(nextProps, nextState) {
    let { showCart } = nextProps;
    if (this.props.showCart != showCart) {
      this.setState(
        {
          isOpen: showCart
        },
        () => {
          console.log(this.state.isOpen);
        }
      );
    }
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (
      this.props.showCart &&
      this.wrapperRef &&
      !this.wrapperRef.contains(event.target)
    ) {
        console.log(event)
      this.props.actions.hideCart();
    }
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    showCart: state.app.showCart
  };
};
export default connectRedux(mapStateToProps, CartScreen);
