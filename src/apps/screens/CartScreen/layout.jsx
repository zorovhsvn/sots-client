import React, { Component } from "react";
import { Button, Container, Image } from "react-bootstrap";
import "./style.css";
import posed from "react-pose";
import FadeIn from "react-fade-in";
import { Link } from "react-router-dom";

const Sidebar = posed.div({
  open: {
    x: "-100%",
    delayChildren: 200,
    staggerChildren: 50
  },
  closed: { x: "0%" }
});

export default class layout extends Component {
  renderItemCart = () => {
    return (
      <React.Fragment>
        <Container
          style={{
            display: "flex",
            flexDirection: "row",
            height: 100,
            marginTop: 10,
            marginBottom: 10
          }}
        >
          <Image
            width={100}
            src={
              "https://cdn.shopify.com/s/files/1/1404/4249/products/giay-bup-be-tre-em-dong-hai-zucia-GTEAK08-nau-3_240x.jpg?v=1576664988"
            }
          />
          <Container>
            <div className={"product-price"}>
              <Link
                to={"/"}
                className={"txt-product-detail"}
                style={{
                  color: "#464647"
                }}
              >
                Giày búp bêGiày búp bêGiày búp bêGiày búp bêGiày búp bêGiày búp
                bêGiày{" "}
              </Link>
            </div>
            <div className={"product-price"}>
              <span>Nâu/31</span>
            </div>
            <div className={"product-price"}>
              <span>390.000 VNĐ</span>
            </div>

            <div
              style={{
                marginTop: 8
              }}
              className={"product-actions d-flex flex-row"}
            >
              <div
                className={"product-quatity"}
                style={{
                  border: "1px solid #CCCC",
                  width: "35%"
                }}
              >
                <Button
                  variant={"link"}
                  style={{
                    backgroundColor: "rgba(255,255,255,0)",
                    color: "#979797",
                    textDecoration: "none"
                  }}
                >
                  -
                </Button>
                <input
                  type={"number"}
                  style={{
                    textAlign: "center",
                    width: "35%",
                    backgroundColor: "rgba(255,255,255,0)",
                    color: "#979797",
                    border: 0
                  }}
                />
                <Button
                  variant={"link"}
                  style={{
                    backgroundColor: "rgba(255,255,255,0)",
                    color: "#979797",
                    textDecoration: "none"
                  }}
                >
                  +
                </Button>
              </div>
              <div
                style={{
                  marginLeft: "auto"
                }}
                className={"product-delete-cart"}
              >
                <Button
                  variant={"link"}
                  style={{
                    color: "#979797",
                    textDecoration: "underline",
                    fontSize: 12
                  }}
                >
                  Xoá
                </Button>
              </div>
            </div>
          </Container>
        </Container>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div
          className={`PageOverlay ${this.state.isOpen ? "is-visible" : ""}`}
        />
        <div
          ref={this.setWrapperRef}
          className={"modal-cart"}
          style={{
            position: "fixed",
            top: 0,
            right: 0,
            height: "100%",
            zIndex: 20
          }}
        >
          <Sidebar
            className="sidebar"
            pose={this.state.isOpen ? "open" : "closed"}
          >
            <div className={"header-modal"}>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "row",
                  flex: 1
                }}
                className={"title-modal p-md-3"}
              >
                <span
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    flex: 1,
                    fontSize: 20,
                    fontFamily: ""
                  }}
                >
                  Giỏ hàng
                </span>

                <Button
                  style={{
                    color: "#000",
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                  onClick={() => this.props.actions.hideCart()}
                  variant={"link"}
                >
                  <i className={"fa fa-close"} />
                </Button>
              </div>
              <hr className={"dash"} />
            </div>
            <div
              style={{
                flex: 4,
                overflow: "auto"
              }}
              className={"cart-item"}
            >
              {this.renderItemCart()}
              {this.renderItemCart()}
              {this.renderItemCart()}
              {this.renderItemCart()}
              {this.renderItemCart()}
              {this.renderItemCart()}

              {this.renderItemCart()}
            </div>
            <hr className={"dash"} />

            <div
              style={{
                flex: 1
              }}
              className={"cart-footer"}
            >
              <div className={"total-price d-flex flex-row mx-md-2"}>
                <div
                  style={{
                    marginRight: "auto"
                  }}
                >
                  <p>Tổng cộng</p>
                </div>
                <div className={""}>
                  <p>700.000 VNĐ</p>
                </div>
              </div>
              <div className={"d-flex justify-content-center"}>
                <Button className={"btn-pay"} variant={"outline-dark"}>
                  Thanh toán ngay
                </Button>
              </div>
            </div>
          </Sidebar>
        </div>
      </React.Fragment>
    );
  }
}
