import Layout from "./layout";
import connectRedux from "../../redux/connect";

class ProductDetailScreen extends Layout {
  constructor(props) {
    super(props);
    this.state = {
      showModalSizeGuide:false,
    };
  }


  showModalSizeGuide = ()=>{
    this.setState({
      showModalSizeGuide:!this.state.showModalSizeGuide
    })
  }

  // END CLASS
}

const mapStateToProps = () => {
  return {};
};

export default connectRedux(mapStateToProps, ProductDetailScreen);
