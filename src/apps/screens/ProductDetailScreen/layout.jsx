import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Image,
  Button,
  Modal,
  Table
} from "react-bootstrap";
import { CarouselComponent, NavBar, FooterComponent } from "../../component";
import "./style.css";
import { Link } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import ReactImageZoom from "react-image-zoom";

export default class Layout extends Component {
  modalGuideSize = () => {
    let { showModalSizeGuide } = this.state;
    return (
      <React.Fragment>
        <Modal
          show={showModalSizeGuide}
          onHide={() => this.showModalSizeGuide()}
          size={"lg"}
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Body>
            <div
              className={"titleHeader"}
              style={{
                textAlign: "center"
              }}
            >
              <h4>SHOES SIZE GUIDE</h4>
              <span className={"text-modal"}>
                Need help with sizing? We are here to help you 7 days a week:
                12pm - 7pm EST
              </span>
              <p className={"text-modal"}>support@flightclub.com</p>
            </div>
            <hr className={"dots"} />
            <Container>
              <Row>
                <Col md={6}>
                  <div
                    style={{
                      textAlign: "center",
                      fontWeight:'bold'

                    }}
                    className={"title-header p-md-2"}
                  >
                    <span>MEN</span>
                  </div>
                  <Table responsive="xl">
                    <thead>
                      <tr>
                        <th>US</th>
                        <th>UK</th>
                        <th>EUROPE</th>
                        <th>CM</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>6</td>
                        <td>5.5</td>
                        <td>38.5</td>
                        <td>24</td>
                      </tr>
                      <tr>
                        <td>6.5</td>
                        <td>6</td>
                        <td>39</td>
                        <td>24.5</td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td>6</td>
                        <td>40</td>
                        <td>25</td>
                      </tr>
                      <tr>
                        <td>7.5</td>
                        <td>6.5</td>
                        <td>40.5</td>
                        <td>25.5</td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td>7</td>
                        <td>41</td>
                        <td>26</td>
                      </tr>
                      <tr>
                        <td>8.5</td>
                        <td>7.5</td>
                        <td>42</td>
                        <td>26.5</td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td>8</td>
                        <td>42.5</td>
                        <td>27</td>
                      </tr>
                      <tr>
                        <td>9.5</td>
                        <td>8.5</td>
                        <td>43</td>
                        <td>27.5</td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td>9</td>
                        <td>44</td>
                        <td>28</td>
                      </tr>
                      <tr>
                        <td>10.5</td>
                        <td>9.5</td>
                        <td>44.5</td>
                        <td>28.5</td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td>10</td>
                        <td>45</td>
                        <td>29</td>
                      </tr>
                      <tr>
                        <td>11.5</td>
                        <td>10.5</td>
                        <td>45.5</td>
                        <td>29.5</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td>11</td>
                        <td>46</td>
                        <td>30</td>
                      </tr>
                    </tbody>
                  </Table>
                </Col>
                <Col md={6}>
                  <div
                    style={{
                      textAlign: "center",
                      fontWeight:'bold'
                    }}
                    className={"title-header p-md-2"}
                  >
                    <span>WOMAN</span>
                  </div>
                  <Table responsive="xl">
                    <thead>
                      <tr>
                        <th>US</th>
                        <th>UK</th>
                        <th>EUROPE</th>
                        <th>CM</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>4</td>
                        <td>1.5</td>
                        <td>34.5</td>
                        <td>21</td>
                      </tr>
                      <tr>
                        <td>4.5</td>
                        <td>2</td>
                        <td>35</td>
                        <td>21.5</td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>2.5</td>
                        <td>35.5</td>
                        <td>22</td>
                      </tr>
                      <tr>
                        <td>5.5</td>
                        <td>3</td>
                        <td>36</td>
                        <td>22.5</td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>3.5</td>
                        <td>36.5</td>
                        <td>23</td>
                      </tr>
                      <tr>
                        <td>6.5</td>
                        <td>4</td>
                        <td>37.5</td>
                        <td>23.5</td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td>4.5</td>
                        <td>38</td>
                        <td>24</td>
                      </tr>
                      <tr>
                        <td>7.5</td>
                        <td>5</td>
                        <td>38.5</td>
                        <td>24.5</td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td>5.5</td>
                        <td>39</td>
                        <td>25</td>
                      </tr>
                      <tr>
                        <td>8.5</td>
                        <td>6</td>
                        <td>40</td>
                        <td>25.5</td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td>6.5</td>
                        <td>40.5</td>
                        <td>26</td>
                      </tr>
                      <tr>
                        <td>9.5</td>
                        <td>7</td>
                        <td>41</td>
                        <td>26.5</td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td>7.5</td>
                        <td>42</td>
                        <td>27</td>
                      </tr>
                      <tr>
                        <td>10.5</td>
                        <td>8</td>
                        <td>42.5</td>
                        <td>27.5</td>
                      </tr>
                      <tr>
                        <td>11</td>
                        <td>8.5</td>
                        <td>43</td>
                        <td>28</td>
                      </tr>
                      <tr>
                        <td>11.5</td>
                        <td>9</td>
                        <td>44</td>
                        <td>28.5</td>
                      </tr>
                      <tr>
                        <td>12</td>
                        <td>9.5</td>
                        <td>44.5</td>
                        <td>29</td>
                      </tr>
                      <tr>
                        <td>12.5</td>
                        <td>10</td>
                        <td>45</td>
                        <td>29.5</td>
                      </tr>
                    </tbody>
                  </Table>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  };

  itemProduct = () => {
    return (
      <React.Fragment>
        <Link
          to={"/"}
          className={"product-details"}
          style={{
            textAlign: "center",
            backgroundColor: "#FFF",
            textDecoration: "none"
          }}
        >
          <Image
            thumbnail
            src={
              "https://thumbs.nosto.com/quick/h239k2p6/8/344297/82b573addfd423fe14f973e595128cbdb1296e58ccdccbe352a638c5ddc240efa/A"
            }
          />
          <Col>
            <p
              className={"product-detail-text"}
              style={{
                fontWeight: "bold",
                fontSize: 18
              }}
            >
              Air Jordan
            </p>

            <p
              className={"product-detail-text"}
              style={{
                color: "#b1b1b1"
              }}
            >
              Detail Jordan
            </p>
            <span>$Price</span>
          </Col>
        </Link>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <div>
        <NavBar
              goToLogin={() => this.props.history.replace("/account")}
              showCart={() =>
                this.props.showCart
                  ? this.props.actions.hideCart()
                  : this.props.actions.showCart()
              }
            />
        </div>

        <Container
          fluid
          style={{
            padding: 0
          }}
        >
          <Row>
            <Col md={8} xs={6}>
              <Carousel showArrows infiniteLoop>
                <div>
                  <img src="https://www.flightclub.com/media/catalog/product/cache/1/small_image/360x257/9df78eab33525d08d6e5fb8d27136e95/1/5/152814_01.jpg" />
                </div>
                <div>
                  <img src="https://image.thanhnien.vn/660/uploaded/thanhlongn/2019_07_02/st1_eoof_brqx.jpg" />
                </div>
              </Carousel>
            </Col>
            <Col md={4} xs={6}>
              <div
                className={"product-detail d-flex"}
                style={{
                  justifyContent: "center",
                  textAlign: "center",
                  fontFamily: "Roboto Condensed,sans-serif",
                  flexDirection: "column",
                  marginRight: 10
                }}
              >
                <div className={"product-name"}>
                  <h2>Sub category</h2>
                  <h3>Product name</h3>
                </div>

                <div className={"product-price"}>
                  <h2>$340</h2>
                </div>

                <div className={"product-size"}>
                  <div
                    style={{
                      display: "flex",
                      flexWrap: "wrap",
                      textAlign: "start"
                    }}
                  >
                    <span
                      style={{
                        flex: 1,
                        alignSelf:'center'
                      }}
                    >
                      Select Size
                    </span>
                    <Button style={{
                      color:'#343a40'
                    }} variant={'link'} onClick={this.showModalSizeGuide}>Size guide</Button>
                  </div>

                  <div className={"custom-size"}>
                    {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((values, i) => {
                      return (
                        <Button
                          key={i}
                          className={"m-md-1"}
                          variant={"outline-dark"}
                        >
                          {" "}
                          size {values}
                        </Button>
                      );
                    })}
                  </div>
                </div>

                <div className={"product-cart mb-md-1"}>
                  <Button
                    style={{
                      width: "100%",
                      padding: 10,
                      borderRadius: 30,
                      margin: "10px 0 10px 0"
                    }}
                    variant={"dark"}
                  >
                    Add to cart
                  </Button>
                </div>

                <div className={"product-Estimated font-code"}>
                  <span>Estimated delivery: 1 week</span>
                </div>

                <div className={"product-details font-code"}>
                  <span>fu9006 | black, black, black | 5/16/19</span>
                </div>

                <div
                  style={{
                    textAlign: "start"
                  }}
                  className={"product-description font-code mt-md-3"}
                >
                  <span>
                    Through evolved design elements and advanced technology, the
                    adidas Yeezy Boost 350 V2 lives up to its cults appeal.
                    Released in June 2019, the re-engineered Primeknit bootie of
                    this "Black Non-Reflective" edition sees futuristic updates
                    including a translucent side stripe and bold heel pull
                    stitching. Integrated lacing customizes fit while the covert
                    feel is supplemented by a translucent black Boost-equipped
                    midsole.
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Container>

        <FooterComponent />

        {/* Modal */}
        {this.modalGuideSize()}
      </React.Fragment>
    );
  }
}
