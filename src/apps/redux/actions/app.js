export const showCart = ()=>{
    return{
        type:"SHOW_CART",
        payload:{}
    }
}

export const hideCart = ()=>{
    return{
        type:"HIDE_CART",
        payload:{}
    }
}
