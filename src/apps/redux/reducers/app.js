const initialState = {
    showCart: false
};

const app = (state = initialState, action)=>{
    switch(action.type){
        case "SHOW_CART":
                return {
                    ...state,
                    showCart:true
                }
        case "HIDE_CART":
        return {
            ...state,
            showCart:false
        }
        default:    
            return state
    }
}

export default app