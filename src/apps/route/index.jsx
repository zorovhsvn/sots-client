import React, { Component } from "react";
import { Route, Link, Switch, BrowserRouter,useHistory } from "react-router-dom";
import { HomeScreen, ProductDetailScreen, LoginScreen,CartScreen, CategoryScreen } from "../screens";
import { LoginScreen as LoginAdminScreen } from "../screens/AdminScreen";

import { AnimatedSwitch } from "react-router-transition";

export default class RouteComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <BrowserRouter history={useHistory}>
          <AnimatedSwitch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper"
          >
            <Route path="/" exact component={HomeScreen} />
            <Route path="/detail" component={ProductDetailScreen} />
            <Route path="/category" component={CategoryScreen} />
            <Route path="/account" component={LoginScreen} />


            {/* AdminScreen */}
            <Route path="/@admincp@" exact component={LoginAdminScreen} />
          </AnimatedSwitch>
          <CartScreen />

        </BrowserRouter>
      </React.Fragment>
    );
  }
}
